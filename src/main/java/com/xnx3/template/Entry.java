package com.xnx3.template;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jsoup.nodes.Entities;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.skin.NebulaSkin;
import org.jvnet.substance.skin.SubstanceOfficeBlue2007LookAndFeel;
import com.xnx3.util.AutoRun;

public class Entry {
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new Runnable() { 
			public void run() {
//				SubstanceImageWatermark watermark = new SubstanceImageWatermark(MainEntry.class.getResourceAsStream("/res/icon.png"));
//				watermark.setKind(ImageWatermarkKind.SCREEN_CENTER_SCALE);
//				SubstanceSkin skin = new OfficeBlue2007Skin().withWatermark(watermark);   //初始化有水印的皮肤
				
				try{
					SubstanceLookAndFeel.setSkin(new NebulaSkin());
//					SubstanceLookAndFeel.setSkin(skin);
				}catch(Exception e){
					e.printStackTrace();
				}
				

				Global.mainUI.setVisible(true);
				Global.mainUI.getLblNewLabel_log().setText("<html><div style=\"width:100%; text-align:center; font-size:22px;\">点击查看使用说明</div>");
				AutoRun.versionCheck();//版本检测
				
				
//				MainJframe mainJframe = new MainJframe();
//				mainJframe.setSize(425, 295);
//				mainJframe.setVisible(true);
				
//				new Tray();	//创建托盘
				
				//新版本检测
				//VersionCheck.check("http://version.xnx3.com/shopStorePCClient.html", Global.VERSION, "storeClient");
			}
		});
	}
}
